#!/bin/bash
#
# This script is executed upon the first `vagrant up` or when running
# `vagrant up --provision`. You may use it to install any additional software
# that you require for your submission.
#
#
set -e

add-apt-repository ppa:jonathonf/python-3.6

wget -q https://packages.microsoft.com/config/ubuntu/14.04/packages-microsoft-prod.deb
dpkg -i packages-microsoft-prod.deb

apt-get install -y apt-transport-https
apt-get update
apt-get install -y dotnet-sdk-2.1
apt-get install -y mysql-client
apt-get install -y aptitude

# I need Python 3.6 for aiohttp
# With various attempts at different methods of upgrading Python.
# This one worked
aptitude install -y python3.6
aptitude install -y python3.6-dev
aptitude install -y python3.6-venv
wget https://bootstrap.pypa.io/get-pip.py
python3.6 get-pip.py
ln -s /usr/bin/python3.6 /usr/local/bin/python3

apt-get update

# The MySql db sync app I wrote needs mysql-utilities
wget -q https://downloads.mysql.com/archives/get/file/mysql-utilities_1.6.5-1debian8_all.deb
aptitude install -y mysql-utilities
rm -rf /var/lib/apt

# Setup Python app
touch /home/vagrant/.bash_aliases && \
echo "alias python=python3.6" > /home/vagrant/.bash_aliases

cd /home/vagrant/apps/urlshortener
pip install -e .

# Deploy database
cd ./urlshortener/database
dotnet run option=2

# Import Phishing DB data
cd /home/vagrant/apps/urlshortener
python3 convert_json_to_sql.py

# Start the WebAPI
echo "Starting urlshortener web..."
python3 -m urlshortener.webapi