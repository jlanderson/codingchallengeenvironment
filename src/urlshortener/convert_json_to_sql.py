import mysql.connector
import ijson
import os
import sys

from urllib.parse import urlparse

INSERT_SQL = "INSERT INTO phish (phishId, domain, url) VALUES %s"
PHISH_ID = 'phish_id'
PHISH_URL = 'url'

file_size = os.path.getsize('verified_online.json')
connection = mysql.connector.connect(host='localhost', port=3306, database='urlshortener', user='urlshortener', password='urlshortener', autocommit=True)

records_written = 0

phish_id = None
phish_netloc = None
phish_url = None
phish_list = []

with open('verified_online.json', 'r') as f:
    try:
        print('Loading phishing data. This may take a few minutes.')
        phishes = ijson.parse(f)
        for prefix, event, value in phishes:
            if prefix == 'item.phish_id':
                phish_id = value
            elif prefix == 'item.url':
                phish_url = value
                parsedUrl = urlparse(phish_url)
                phish_netloc = parsedUrl.netloc
            elif (prefix, event) == ('item', 'end_map'):
                if (phish_id and phish_netloc and phish_url):
                    data = (phish_id, phish_netloc, phish_url)
                    phish_list.append("(%s, '%s', '%s') " % data)
                    records_written += 1
                    phish_id = None
                    phish_netloc = None
                    phish_url = None
                    if records_written % 100 == 0:
                        cursor = connection.cursor()
                        cursor.execute(INSERT_SQL % ','.join(phish_list).replace("''", "'"))
                        cursor.close()
                        phish_list = []
                        sys.stdout.write("%s phishes written to the database   \r" % (records_written))
                        sys.stdout.flush()
        if len(phish_list) > 0:
            cursor = connection.cursor()
            cursor.execute(INSERT_SQL % ','.join(phish_list).replace("''", "'"))
            cursor.close()
            phish_list = []

    except Exception as e:
        print(e)
        f.close()
        connection.close()
    
    f.close()
print("%s phishes written to the database" % (records_written))
connection.close()
    
