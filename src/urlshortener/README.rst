UrlShortener
============

UrlShortener
++++++++++++
* http://127.0.0.1:8088 - This is the front-end in PHP
* http://192.168.33.10:8089 - This is the api done in Python

Forgive my Python Noobiness
+++++++++++++++++++++++++++
Even though I've only ever edited a few .py files in my life and I have never worked on a full Python solution, I decided to go ahead and do this solution in Python. I tried to research what conventions Pythoners use when organizing a solution, but my lack of experience in Python solutions my be apparent. Though, hopefully, my flexibility and aptitude are also apparent with this decision.

Utility for MySql Included
++++++++++++++++++++++++++
The "database" folder is not a Python package but is actually a .Net Core application and includes a utility I wrote for updating MySql databases based on SQL schema files. It can also do the reverse and update the schema files based on the database. It can also render data models, view models, persistent reference data, and database context files. It can probably be ported to Python fairly easily as it is mostly just shell calls. I do intend to release this utility under an open source license on GitHub at some point (probably after I've abstracted the configuration a bit more).

Deployment
++++++++++
Vagrant should automatically deploy the database and run this Python application

Dev Requirements
++++++++++++++++
* .Net Core 2.1
* dotnet script
* MySql Utilities

Runtime Requirements
++++++++++++++++++++
* Python 3.5+
* aiohttp
* aiohttp-cors
* ijson
* mysql-connector
* pyyaml

Reading
+++++++
There were a few concepts I had to read up on. I thought I'd share my sources here.

Web/App Server Performance::
    http://klen.github.io/py-frameworks-bench/
    http://www.artificialworlds.net/blog/2017/06/12/making-100-million-requests-with-python-aiohttp/
    https://github.com/samuelcolvin/aiohttp-vs-sanic-vs-japronto

Folder/File Conventions::
    https://docs.python-guide.org/writing/structure/
    https://realpython.com/python-application-layouts/
