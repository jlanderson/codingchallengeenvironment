import mysql.connector
from urllib.parse import urlparse

class DbContext:

    def __init__(self, conf):
        self.conf = conf
        dbConf = conf['database']
        self.connection = mysql.connector.connect(
            host=dbConf['host'], 
            port=dbConf['port'], 
            database=dbConf['name'], 
            user=dbConf['user'], 
            password=dbConf['pass'], 
            autocommit=True,
            buffered=True)
        self.slugs = DbContext.Slugs(self.connection)
        self.phishes = DbContext.Phishes(self.connection)

    class Slugs:
        # TODO: Check index optimization
        def __init__(self, connection):
            self.connection = connection
            self.USABLE_CHARS = "abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ-0123456789"
            self._base = len(self.USABLE_CHARS)
                
        def encode(self, id):
            if id == 0:
                return self.USABLE_CHARS[0]
            arr = []
            base = len(self.USABLE_CHARS)
            while id:
                id, rem = divmod(id, base)
                arr.append(self.USABLE_CHARS[rem])
            return ''.join(arr)

        def decode(self, slug):            
            id = 0
            for i in range(len(slug) - 1, -1, -1):
                c = ord(slug[i])
                if 97 <= c and c <= 122: # 'a' to 'z'
                    id = id * self._base + c - 97
                if 65 <= c and c <= 90: # 'A' to 'Z'
                    id = id * self._base + c - 65 + 27
                if 48 <= c and c <= 57: # '0' to '9'
                    id = id * self._base + c - 48 + 54
                if c == 45: # '-' (dash)
                    id = id * self._base + 53
                if c == 95: # '_' (underscore)
                    id = id * self._base + 26
            return id

        async def getUrlBySlug(self, slug):
            id = self.decode(slug)
            sql = 'SELECT urlId, url FROM url WHERE urlId = %s'
            data = (id,)
            cursor = self.connection.cursor(dictionary=True)
            cursor.execute(sql, data)
            url = cursor.fetchone()['url']
            cursor.close()
            return url

        async def createSlugAndSaveUrl(self, url):
            sql = 'INSERT INTO url (url) VALUES (%s)'
            data = (url,)
            cursor = self.connection.cursor()
            cursor.execute(sql, data)
            id = cursor.lastrowid
            cursor.close()
            slug = self.encode(id)
            return slug
    
    class Phishes:

        # TODO: Check index optimization
        def __init__(self, connection):
            self.connection = connection
        
        async def getPhishByUrl(self, url):
            sql = 'SELECT phishId FROM phish WHERE domain = %s AND url = %s'
            data = (url, urlparse(url).netloc)
            cursor = self.connection.cursor(dictionary=True)
            cursor.execute(sql, data)
            if cursor.rowcount < 1:
                cursor.close()
                return 0
            phish = cursor.fetchone()['phishId']
            cursor.close()
            return phish
            