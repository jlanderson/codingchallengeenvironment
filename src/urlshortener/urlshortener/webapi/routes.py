import aiohttp_cors

def setup_routes(app, handler, cors, project_root):
    cors.add(app.router.add_route("GET", '/{slug}', handler.redirect))
    cors.add(app.router.add_route("POST", '/shorten', handler.shorten))
    