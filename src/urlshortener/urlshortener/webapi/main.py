import aiohttp_cors
import asyncio
import logging
import pathlib
import yaml

from aiohttp import web
from urlshortener.webapi import actions, routes, dbcontext

ROOT_DIR = pathlib.Path(__file__).parent.parent

async def init(loop):
    conf = get_config(ROOT_DIR / 'webapi' / 'config' / 'config.yml')

    app = web.Application(loop=loop)
    
    # This should be locked down before production
    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*",
                allow_methods="*"
            )
    })

    db = dbcontext.DbContext(conf)

    _actions = actions.Actions(db, conf)

    routes.setup_routes(app, _actions, cors, ROOT_DIR)
    host, port = conf['host'], conf['port']
    return app, host, port

def main():
    logging.basicConfig(level=logging.DEBUG)

    loop = asyncio.get_event_loop()
    app, host, port = loop.run_until_complete(init(loop))

    # TODO: Global Error Filtering?
    web.run_app(app, host=host, port=port)

def get_config(configFile):
    with open(configFile, 'rt') as f:
        data = yaml.load(f)
    return data
    
if __name__ == '__main__':
    main()