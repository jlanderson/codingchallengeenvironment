import aiohttp.web as web
from urllib.parse import urlparse
import json

class Actions:

    def __init__(self, db, conf):
        self.db = db
        self.conf = conf

    async def shorten(self, request):
        res = await request.json()
        fullUrl = res["url"]

        # TODO: Better async usage - asyncio.gather?

        # Validate URL
        await self.validateUrl(fullUrl)

        # Validate Phishing
        await self.checkForPhishing(fullUrl)

        slug = await self.db.slugs.createSlugAndSaveUrl(fullUrl)
        
        shortUrl = "http://{host}:{port}/{slug}".format(host=self.conf['host'], port=self.conf['port'], slug=slug)

        return web.json_response({"url": shortUrl})

    async def redirect(self, request):
        slug = request.match_info['slug']
        location = await self.db.slugs.getUrlBySlug(slug)
        if not location:
            raise web.HTTPNotFound()
        return web.HTTPFound(location=location)

    async def validateUrl(self, url):
        parsedUrl = urlparse(url)
        if not parsedUrl.scheme or not parsedUrl.netloc:
            raise web.HTTPBadRequest(text=json.dumps({'message': 'Invalid Target URL'}))

    async def checkForPhishing(self, url):
        phish = await self.db.phishes.getPhishByUrl(url)
        print('phish==0 is %s' % (phish == 0,))
        if phish:
            raise web.HTTPNotAcceptable(text=json.dumps({'message': 'This URL is associated with known phishing attempts', 'id': phish }))
