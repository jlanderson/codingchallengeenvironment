﻿#! "netcoreapp2.0"
#r "nuget:MySql.Data,6.10.6"
#r "nuget:NetStandard.Library,2.0.0"
#r "nuget:Newtonsoft.Json,10.0.3"
#r "nuget:System.Collections ,4.3.0"
#r "nuget:System.Data.Common,4.3.0"
#r "nuget:System.IO.FileSystem,4.3.0"
using MySql.Data;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Text;

var settings = JsonConvert.DeserializeObject<dynamic>(File.ReadAllText($"entitysettings.json"));

string connectionString = Environment.GetEnvironmentVariable("CONNECTION_STRING");
string solutionProjectsDirectory = Environment.GetEnvironmentVariable("SOLUTION_PROJECTS_FOLDER");
string entitiesOutputDirectory = string.Empty;

if (connectionString == null)
{
    connectionString = settings.contextSettings.connectionString.ToString();
}
if (solutionProjectsDirectory == null)
{
    solutionProjectsDirectory = "..\\";
}

entitiesOutputDirectory = settings.entitySettings.outputDirectory.ToString().Replace("{solutionProjectsDirectory}", solutionProjectsDirectory);

IEnumerable <dynamic> overrides = settings.entitySettings.overrides;

List<string> tableNames = new List<string>();
StringBuilder context = new StringBuilder($"using System;\r\nusing Microsoft.EntityFrameworkCore;\r\nusing {settings.entitySettings.nameSpace};\r\n\r\nnamespace {settings.contextSettings.nameSpace}\r\n{{\r\n\tpublic partial class {settings.contextSettings.className} : DbContext\r\n\t{{\r\n\t\tpublic static string ConnectionString {{ get; set; }}\r\n");
StringBuilder modelBuilder = new StringBuilder();

using (var connection = new MySqlConnection(connectionString))
{
    var tableCommand = new MySqlCommand($"SELECT TABLE_SCHEMA, TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA = '{settings.contextSettings.databaseName}'", connection);
    Console.WriteLine(tableCommand.CommandText);
    connection.Open();
    using (var tableReader = tableCommand.ExecuteReader())
    {
        while (tableReader.Read())
        {
            if (!overrides.Any(o => o.type == "table" && o.name == (string) tableReader[1] && o.doNotGenerate?.Value))
            {
                tableNames.Add(tableReader[1].ToString());
            }
        }
    }

    if (!Directory.Exists(entitiesOutputDirectory))
    {
        Directory.CreateDirectory(entitiesOutputDirectory);
    }

    foreach (var table in tableNames)
    {
        string tableWithoutSchemaName = RemoveSchemaName(table);
        string cleanedTableName = SnakeCaseToPascalCase(tableWithoutSchemaName);
        context.AppendLine($"\t\tpublic virtual DbSet<{cleanedTableName}> {cleanedTableName}s {{ get; set; }}");

        StringBuilder model = new StringBuilder($"using System;\r\nusing System.Collections.Generic;\r\n\r\n");
        model.AppendLine($"namespace {settings.entitySettings.nameSpace}\r\n{{\r\n\tpublic partial class {cleanedTableName} : UpdateableEntityBase\r\n\t{{");
        
        StringBuilder children = new StringBuilder();
        StringBuilder parent = new StringBuilder();
        StringBuilder fields = new StringBuilder();
        StringBuilder constr = new StringBuilder();

        modelBuilder.AppendLine($"\t\t\tmodelBuilder.Entity<{cleanedTableName}>(entity =>\r\n\t\t\t{{");
        
        modelBuilder.AppendLine($"\t\t\t\tentity.ToTable(\"{table}\");");

        // This gets our fields
        var columnCommand = new MySqlCommand($"SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_TYPE, COLUMN_KEY, EXTRA FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{table}' AND TABLE_SCHEMA = '{settings.contextSettings.databaseName}'", connection);
        Console.WriteLine(columnCommand.CommandText);
        using (var columnReader = columnCommand.ExecuteReader())
        {
            while (columnReader.Read())
            {
                string columnType = null;
                if(columnReader[1].ToString().ToLower() == "varchar" && columnReader[3].ToString().ToLower() == "varchar(64)")
                {
                    columnType = "Guid";
                }
                else if(columnReader[1].ToString().ToLower().Contains("decimal") || columnReader[1].ToString().ToLower().Contains("money") || columnReader[1].ToString().ToLower().Contains("numeric"))
                {
                    columnType = "decimal";
                }
                else if(columnReader[1].ToString().ToLower() == "bit" || (columnReader[1].ToString().ToLower() == "tinyint" && columnReader[3].ToString().ToLower() == "tinyint(1)"))
                {
                    columnType = "bool";
                }
                else if(columnReader[1].ToString().ToLower().Contains("date"))
                {
                    columnType = "DateTime";
                }
                else if(columnReader[1].ToString().ToLower().Contains("binary") || columnReader[1].ToString().ToLower().Contains("blob"))
                {
                    columnType = "byte[]";
                }
                else if (columnReader[1].ToString().ToLower().EndsWith("int"))
                {
                    columnType = "int";
                }
                if (columnType != null)
                {
                    columnType += (columnReader[2].ToString() == "YES" && !columnType.EndsWith("[]")) || (columnReader[4]?.ToString() == "PRI" && columnReader[5]?.ToString() == "auto_increment") ? "?" : string.Empty;
                }
                else
                {
                    columnType = "string";
                }
                if (overrides.Any(o => o.type=="field" && o.name==$"{cleanedTableName}.{columnReader[0]}" && o.fieldType?.ToString().Length > 0))
                {
                    columnType = overrides.SingleOrDefault(o => o.type=="field" && o.name==$"{cleanedTableName}.{columnReader[0]}").fieldType.ToString();
                }
                fields.AppendLine($"\t\tpublic {columnType} {columnReader[0]} {{ get; set; }}");

            }
        }

        // This picks up on children relationships with other tables
        var childrenCommand = new MySqlCommand($"SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_NAME = '{table}' AND REFERENCED_TABLE_SCHEMA = '{settings.contextSettings.databaseName}'", connection);
        Console.WriteLine(childrenCommand.CommandText);
        using (var childrenReader = childrenCommand.ExecuteReader())
        {
            while (childrenReader.Read())
            {
                string tableName = SnakeCaseToPascalCase(RemoveSchemaName(childrenReader[5].ToString()));
                children.AppendLine($"\t\tpublic virtual ICollection<{tableName}> {tableName}s {{ get; set; }}");
                constr.AppendLine($"\t\t\t{tableName}s = new HashSet<{tableName}>();");
            }
        }

        //TODO: One-to-one relationships?

        // This picks up on parent relationships with other tables
        var parentCommand = new MySqlCommand($"SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE CONSTRAINT_NAME <> 'PRIMARY' AND TABLE_NAME = '{table}' AND TABLE_SCHEMA = '{settings.contextSettings.databaseName}'", connection);
        Console.WriteLine(parentCommand.CommandText);
        using (var parentReader = parentCommand.ExecuteReader())
        {
            while (parentReader.Read())
            {
                string tableName = SnakeCaseToPascalCase(RemoveSchemaName(parentReader[10].ToString()));
                if (!string.IsNullOrWhiteSpace(tableName))
                {
                    parent.AppendLine($"\t\tpublic virtual {tableName} {tableName} {{ get; set; }}");
                    modelBuilder.AppendLine($"\t\t\t\tentity.HasOne(d => d.{tableName}).WithMany(p => p.{cleanedTableName}s).HasForeignKey(d => d.{parentReader[6].ToString()});");
                }
            }
        }

        // Create relationships manually via overrides

        // A "reference" relationship (foreign)
        if (overrides.Any(o => o.type == "relationship" && o.foreign.table == $"{cleanedTableName}" && o.relationship == "reference"))
        {
            var relationship = overrides.SingleOrDefault(o => o.type == "relationship" && o.foreign.table == $"{cleanedTableName}" && o.relationship == "reference");
            parent.AppendLine($"\t\tpublic virtual {relationship.primary.table} {relationship.primary.table} {{ get; set; }}");
            //modelBuilder.AppendLine($"\t\t\t\tentity.HasOptional(d => d.{relationship.primary.table}).WithMany(p => p.{relationship.foreign.table}s).HasForeignKey(d => d.{relationship.foreign.key});");
        }

        // A "reference" relationship (primary)
        if (overrides.Any(o => o.type == "relationship" && o.primary.table == $"{cleanedTableName}" && o.relationship == "reference"))
        {
            var relationship = overrides.SingleOrDefault(o => o.type == "relationship" && o.primary.table == $"{cleanedTableName}" && o.relationship == "reference");
            children.AppendLine($"\t\tpublic virtual ICollection<{relationship.foreign.table}> {relationship.foreign.table}s {{ get; set; }}");
            constr.AppendLine($"\t\t\t{relationship.foreign.table}s = new HashSet<{relationship.foreign.table}>();");
        }

        // If there were children, we'll need to instantiate them in the entity contructor
        if (constr.Length > 0)
        {
            model.AppendLine($"\t\tpublic {cleanedTableName}()\r\n\t\t{{");
            model.Append(constr.ToString());
            model.AppendLine($"\t\t}}");
        }
        if (fields.Length > 0)
        {
            model.Append(fields.ToString());
        }
        if (parent.Length > 0)
        {
            model.Append(parent.ToString());
        }
        if (children.Length > 0)
        {
            model.Append(children.ToString());
        }
        model.AppendLine($"\t}}\r\n}}");

        // Render the Entity models
        File.WriteAllText($"{settings.entitySettings.outputDirectory.ToString().Replace("{solutionProjectsDirectory}", solutionProjectsDirectory)}/{cleanedTableName}.Generated.cs", model.ToString());  
        
        modelBuilder.AppendLine($"\t\t\t}});\r\n");
      
    }
    context.AppendLine($"\r\n\t\tprotected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)\r\n\t\t{{\r\n\t\t\toptionsBuilder.UseMySql(ConnectionString, mysqlOptions => {{ mysqlOptions.ServerVersion(new Version(5, 6, 41), ServerType.MySql); }}););\r\n\t\t}}}}");
    context.AppendLine($"\r\n\t\tprotected override void OnModelCreating(ModelBuilder modelBuilder)\r\n\t\t{{\r\n");
    context.AppendLine(modelBuilder.ToString());
    context.AppendLine($"\t\t}}");
    context.AppendLine($"\t}}\r\n}}\r\n");

    File.WriteAllText($"{settings.contextSettings.outputDirectory.ToString().Replace("{solutionProjectsDirectory}", solutionProjectsDirectory)}/{settings.contextSettings.className}.Generated.cs", context.ToString());
}

string RemoveSchemaName(string input)
{
    return String.Join("_", input.Split('_').ToList().Where((source, index) => index != 0));
}

string SnakeCaseToPascalCase(string snakeString)
{
    StringBuilder resultBuilder = new StringBuilder();

    foreach (char c in snakeString)
    {
        // Replace anything, but letters and digits, with space
        if (!Char.IsLetterOrDigit(c))
        {
            resultBuilder.Append(" ");
        }
        else
        {
            resultBuilder.Append(c);
        }
    }

    string result = resultBuilder.ToString();

    // Make result string all lowercase, because ToTitleCase does not change all uppercase correctly
    result = result.ToLower();

    // Creates a TextInfo based on the "en-US" culture.
    TextInfo myTI = new CultureInfo("en-US", false).TextInfo;

    result = myTI.ToTitleCase(result).Replace(" ", String.Empty);
    return result;
}