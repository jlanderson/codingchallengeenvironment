﻿#! "netcoreapp2.0"
#r "nuget:NetStandard.Library,2.0.0"
#r "nuget:Newtonsoft.Json,10.0.3"
#r "nuget:System.IO.FileSystem,4.3.0"
#r "nuget:System.Text.RegularExpressions,4.3.0"
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text.RegularExpressions;

string solutionProjectsFolder = Environment.GetEnvironmentVariable("SOLUTION_PROJECTS_FOLDER");
Console.WriteLine($"SOLUTION_PROJECTS_FOLDER env var in CreateDTOsByIO.csx is {solutionProjectsFolder ?? "null"}");

string outputDirectory = string.Empty;
string sourceDirectory = string.Empty;

if (solutionProjectsFolder == null)
{
    solutionProjectsFolder = "..\\";
}

var settings = JsonConvert.DeserializeObject<dynamic>(File.ReadAllText($"dtosettings.json"));

outputDirectory = settings.dtoSettings.outputDirectory.ToString().Replace("{solutionProjectsFolder}", solutionProjectsFolder);

if (!outputDirectory.EndsWith("\\"))
{
    outputDirectory += "\\";
}

sourceDirectory = settings.dtoSettings.sourceEntitiesPath.ToString().Replace("{solutionProjectsFolder}", solutionProjectsFolder);

IEnumerable<dynamic> overrides = settings.dtoSettings.overrides;

if (!Directory.Exists(outputDirectory))
{
    Directory.CreateDirectory(outputDirectory);
}

Console.WriteLine($"ViewModels source directory is {sourceDirectory}. The output directory is {outputDirectory}.");

var source = new DirectoryInfo(sourceDirectory);

foreach (var file in source.GetFiles())
{
    Console.WriteLine($"Processing {file.FullName}...");
    if (file.Name.Contains("Version.Generated") && !file.Name.Contains("MetaData"))
    {
        string entityName = file.Name.Replace("Version.Generated", string.Empty).Replace(".cs", "");

        StringBuilder dto = new StringBuilder($"using System;\r\nnamespace {settings.dtoSettings.nameSpace}\r\n{{\r\n\tpublic partial class {entityName} : IRestModel\r\n\t{{\r\n");

        var entityGenerated = File.ReadAllText(file.FullName);

        //Check to see if there is another partial that's not .Generated
        if (File.Exists(file.FullName.Replace(".Generated", "")))
        {
            entityGenerated += "\r\n" + File.ReadAllText(file.FullName.Replace(".Generated", ""));
        }

        //foreach (var prop in entity.GetProperties().Where(p => !p.Name.Contains("Version") && (p.PropertyType.IsValueType || p.PropertyType == typeof(string)))) // We only need simple types
        foreach (Match prop in Regex.Matches(entityGenerated, @"public\s(?!virtual|partial\s)(?<propType>[^\s\[\]]+)\s(?<propName>[^\s\r\n]+)[\s\r\n]", RegexOptions.Multiline))
        {
            // If it's nullable get the base type
            string type = prop.Groups["propType"].Value;

            if (type.Contains("Nullable"))
            {
                type = type.Split("<>".ToCharArray())[1];
            }

            if (overrides.Any(o => o.type == "type" && o.name == type.TrimEnd('?') && o.replace?.ToString().Length > 0))
            {
                type = overrides.SingleOrDefault(o => o.type == "type" && o.name == type.TrimEnd('?')).replace.ToString();
            }

            if (!type.ToLower().Contains("string") && !type.EndsWith("?"))
            {
                // Make them all Nullable
                type += "?";
            }

            string propName = prop.Groups["propName"].Value;

            dto.AppendLine($"\t\tpublic {type} {propName} {{ get; set; }}");
        }

        dto.AppendLine($"\t}}\r\n}}");

        // Render the DTOs
        File.WriteAllText($"{outputDirectory}{settings.dtoSettings.fileName.ToString().Replace("{classname}", entityName)}", dto.ToString());
    }
}
