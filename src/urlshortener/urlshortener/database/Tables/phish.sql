CREATE TABLE `phish` (
  `phishId` int(11) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `url` varchar(8196) NOT NULL,
  PRIMARY KEY (`phishId`),
  UNIQUE KEY `urlid_UNIQUE` (`phishId`)
) ENGINE=InnoDB AUTO_INCREMENT=12373 DEFAULT CHARSET=latin1;
