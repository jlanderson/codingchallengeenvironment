﻿#! "netcoreapp2.0"
#r "nuget:MySql.Data,6.10.6"
#r "nuget:NetStandard.Library,2.0.0"
#r "nuget:Newtonsoft.Json,10.0.3"
#r "nuget:System.Collections ,4.3.0"
#r "nuget:System.Data.Common,4.3.0"
#r "nuget:System.IO.FileSystem,4.3.0"
using MySql.Data;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Text;

var settings = JsonConvert.DeserializeObject<dynamic>(File.ReadAllText($"referencedatasettings.json"));

string connectionString = Environment.GetEnvironmentVariable("CONNECTION_STRING");
Console.WriteLine($"CONNECTIONSTRING ENV VAR IS '{connectionString ?? "null"}'");
string solutionProjectsDirectory = Environment.GetEnvironmentVariable("SOLUTION_PROJECTS_FOLDER");
Console.WriteLine($"SOLUTION PROJECT DIRECTORY ENV VAR IS '{solutionProjectsDirectory ?? "null"}'");
string databaseProjectFolder = Environment.GetEnvironmentVariable("DATABASE_PROJECT_FOLDER");
Console.WriteLine($"SOLUTION PROJECT DIRECTORY ENV VAR IS '{solutionProjectsDirectory ?? "null"}'");

if (connectionString == null)
{
    connectionString = settings.contextSettings.connectionString.ToString();
}
if (solutionProjectsDirectory == null)
{
    solutionProjectsDirectory = "..\\";
}

List<string> tableNames = new List<string>();

using (var connection = new MySqlConnection(connectionString))
{
    var tableCommand = new MySqlCommand($"SELECT TABLE_SCHEMA, TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA = '{settings.contextSettings.databaseName}'", connection);
    Console.WriteLine(tableCommand.CommandText);
    connection.Open();
    using (var tableReader = tableCommand.ExecuteReader())
    {
        while (tableReader.Read())
        {
            tableNames.Add(tableReader[1].ToString());
        }
    }

    foreach (var table in tableNames)
    {
        string tableWithoutSchemaName = RemoveSchemaName(table);
        string cleanedTableName = SnakeCaseToPascalCase(tableWithoutSchemaName);

        Dictionary<string, string> referenceColumns = new Dictionary<string, string>();

        // This gets our fields
        var columnCommand = new MySqlCommand($"SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{table}' AND TABLE_SCHEMA = '{settings.contextSettings.databaseName}'", connection);
        Console.WriteLine(columnCommand.CommandText);
        using (var columnReader = columnCommand.ExecuteReader())
        {
            while (columnReader.Read())
            {
                string columnType = null;
                if (columnReader[1].ToString().ToLower() == "varchar" && columnReader[3].ToString().ToLower() == "varchar(64)")
                {
                    columnType = "Guid";
                }
                else if (columnReader[1].ToString().ToLower().Contains("decimal") || columnReader[1].ToString().ToLower().Contains("money") || columnReader[1].ToString().ToLower().Contains("numeric"))
                {
                    columnType = "decimal";
                }
                else if (columnReader[1].ToString().ToLower() == "bit" || (columnReader[1].ToString().ToLower() == "tinyint" && columnReader[3].ToString().ToLower() == "tinyint(1)"))
                {
                    columnType = "bool";
                }
                else if (columnReader[1].ToString().ToLower().Contains("date"))
                {
                    columnType = "DateTime";
                }
                else if (columnReader[1].ToString().ToLower().Contains("binary") || columnReader[1].ToString().ToLower().Contains("blob"))
                {
                    columnType = "byte[]";
                }
                else if (columnReader[1].ToString().ToLower().EndsWith("int"))
                {
                    columnType = "int";
                }
                if (columnType != null)
                {
                    columnType += columnReader[2].ToString() == "YES" && !columnType.EndsWith("[]") ? "?" : string.Empty;
                }
                else
                {
                    columnType = "string";
                }

                if (table.StartsWith(settings.referenceDataSettings.referenceDataSchemaName.ToString())) // This is a reference data table, so let's prep the reference data script
                {
                    referenceColumns.Add(columnReader[0].ToString(), columnType);
                }
            }
        }

        if (table.StartsWith(settings.referenceDataSettings.referenceDataSchemaName.ToString()))
        {
            StringBuilder referenceData = new StringBuilder($"USE {settings.contextSettings.databaseName};\r\nSET FOREIGN_KEY_CHECKS=0;\r\nDELETE FROM `{table}`;\r\n");
            StringBuilder refcols = new StringBuilder();
            for (var i = 0; i < referenceColumns.Count; i++)
            {
                refcols.Append($"`{referenceColumns.Keys.ElementAt(i)}`");
                if (i < referenceColumns.Count - 1)
                {
                    refcols.Append(", ");
                }
            }
            var referenceDataCommand = new MySqlCommand($"SELECT {refcols.ToString()} FROM `{table}`", connection);
            Console.WriteLine(referenceDataCommand.CommandText);
            using (var referenceReader = referenceDataCommand.ExecuteReader())
            {
                while (referenceReader.Read())
                {
                    referenceData.AppendLine();
                    referenceData.Append($"INSERT INTO `{table}` ({refcols.ToString()}) VALUES (");
                    for (var i = 0; i < referenceReader.FieldCount; i++)
                    {
                        if (referenceColumns[referenceColumns.Keys.ElementAt(i)] == "decimal" || referenceColumns[referenceColumns.Keys.ElementAt(i)] == "int")
                            referenceData.Append(referenceReader[i].ToString());
                        else
                            referenceData.Append($"'{referenceReader[i].ToString()}'");

                        if (i < referenceReader.FieldCount - 1)
                        {
                            referenceData.Append(", ");
                        }
                    }
                    referenceData.AppendLine(");");
                }
            }
            referenceData.AppendLine("SET FOREIGN_KEY_CHECKS=1;\r\n");

            // Render the reference data file
            File.WriteAllText($"{settings.referenceDataSettings.outputDirectory.ToString().Replace("{solutionProjectsDirectory}", solutionProjectsDirectory)}\\{cleanedTableName}.Generated.sql", referenceData.ToString());
        }
    }
}

string RemoveSchemaName(string input)
{
    return String.Join("_", input.Split('_').ToList().Where((source, index) => index != 0));
}

string SnakeCaseToPascalCase(string snakeString)
{
    StringBuilder resultBuilder = new StringBuilder();

    foreach (char c in snakeString)
    {
        // Replace anything, but letters and digits, with space
        if (!Char.IsLetterOrDigit(c))
        {
            resultBuilder.Append(" ");
        }
        else
        {
            resultBuilder.Append(c);
        }
    }

    string result = resultBuilder.ToString();

    // Make result string all lowercase, because ToTitleCase does not change all uppercase correctly
    result = result.ToLower();

    // Creates a TextInfo based on the "en-US" culture.
    TextInfo myTI = new CultureInfo("en-US", false).TextInfo;

    result = myTI.ToTitleCase(result).Replace(" ", String.Empty);
    return result;
}