﻿using Humanizer;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

namespace UrlShortener.Database.MySql
{
    public class DataTools
    {
        string mySqlServerHost;
        string mySqlAdminUsername;
        static string mySqlAdminPassword;
        bool mySqlCreateUsers;
        bool runScripts;
        string solutionDatabaseProjectFolder;
        string solutionDatabaseProjectEntitiesScript;
        string solutionDatabaseProjectReferenceDataScript;
        string mySqlDatabaseName;
        string solutionProjectsFolder;
        string solutionViewModelsScript;
        static bool isWindows = false;

        IConfigurationRoot configuration;
        StringDictionary envVars = new StringDictionary();
        string mySqlExe = null;
        static bool unattended = false;

        static void Main(string[] args)
        {
            Console.WriteLine($"Running on {RuntimeInformation.OSDescription}");

            isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
            int desiredAction = 0;

            if (args.Length > 0)
            {
                //Console.WriteLine($"DataTools being ran with the arguments: {String.Join(' ', args)}");

                foreach(string arg in args)
                {
                    if (arg.ToLower().Contains("option=") && arg.Length > 7)
                    {
                        int.TryParse(arg.Split('=')[1], out desiredAction);
                    }
                    if (arg.ToLower().Contains("password=") && arg.Length > 9)
                    {
                        mySqlAdminPassword = arg.Split('=')[1];
                    }
                }

                if (desiredAction > 0 && !string.IsNullOrWhiteSpace(mySqlAdminPassword))
                {
                    unattended = true;
                    Console.WriteLine("Option and password are already set. Entering unattended mode.");
                }
            }

            Console.WriteLine();

            if (!unattended)
            {
                while (Array.IndexOf(new[] { 1, 2, 3, 4, 5 }, desiredAction) < 0)
                {
                    Console.WriteLine("Press the number of the action you'd like to take below:\n" +
                        "(1) Sync the project with schema and ReferenceData changes you've made FROM your local database.\n" +
                        "(2) Publish the project TO your local database.\n" +
                        "(3) Update the DomainModels and ViewModels from the database.\n" +
                        "(4) Sync FROM the database, update the DomainModels & ViewModels (1 & 3)\n" +
                        "(5) Publish TO the database, update the DomainModels & ViewModels (2 & 3)\n" +
                        "(6) Only generate the publish database script for review");

                    desiredAction = int.Parse(Console.ReadKey().KeyChar.ToString());
                }
            }

            var datatools = new DataTools();

            int[] actionsRequiringFileUpdates = new int[] { 1, 3, 4, 5};

            datatools.Setup(actionsRequiringFileUpdates.Contains(desiredAction));

            switch (desiredAction)
            {
                case 1:
                    datatools.SyncProjectFromDatabase();
                    break;
                case 2:
                    datatools.PublishTheProjectToLocalDatabase();
                    break;
                case 3:
                    datatools.GenerateModels();
                    break;
                case 4:
                    datatools.SyncProjectFromDatabase();
                    datatools.GenerateModels();
                    break;
                case 5:
                    datatools.PublishTheProjectToLocalDatabase();
                    datatools.GenerateModels();
                    break;
                case 6:
                    datatools.PublishTheProjectToLocalDatabase(false);
                    break;
            }

            Console.WriteLine("Process complete.");
        }

        private void Setup(bool updateProjectFiles = false)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            configuration = builder.Build();

            mySqlServerHost = configuration["Settings:MySql.Server.Host"];
            mySqlAdminUsername = configuration["Settings:MySql.Admin.Username"];
            mySqlCreateUsers = bool.Parse(configuration["Settings:MySql.Users.Create"]);
            solutionDatabaseProjectFolder = configuration["Settings:Solution.DatabaseProject.Folder.Name"];
            solutionDatabaseProjectEntitiesScript = configuration["Settings:Solution.DatabaseProject.EntitiesScript"];
            solutionDatabaseProjectReferenceDataScript = configuration["Settings:Solution.DatabaseProject.ReferenceDataScript"];
            solutionViewModelsScript = configuration["Settings:Solution.ViewModels.Script"];
            mySqlDatabaseName = configuration["Settings:MySql.Database.Name"];
            solutionProjectsFolder = configuration["Settings:Solution.Projects.Folder"];

            if (string.IsNullOrWhiteSpace(mySqlServerHost))
            {
                mySqlServerHost = "localhost";
            }

            if (string.IsNullOrWhiteSpace(mySqlAdminUsername))
            {
                mySqlAdminUsername = "root";
            }

            if (!unattended)
            {
                mySqlAdminPassword = configuration["Settings:MySql.Admin.Password"];                
                if (string.IsNullOrWhiteSpace(mySqlAdminPassword) || mySqlAdminPassword == "[ASK]")
                {
                    Console.WriteLine();
                    Console.Write($"MySQL {mySqlAdminUsername} password:");
                    mySqlAdminPassword = Console.ReadLine();
                }
            }

            if (updateProjectFiles)
            {
                if (string.IsNullOrWhiteSpace(solutionProjectsFolder))
                {
                    // Solution.Projects.Folder wasn't set, so let's see if we can discover where we are by looking for the project folder
                    solutionProjectsFolder = FindProjectsDirectory(AppContext.BaseDirectory);
                }

                if (solutionProjectsFolder.Length > 0 && !solutionProjectsFolder.EndsWith(Path.DirectorySeparatorChar))
                {
                    solutionProjectsFolder += Path.DirectorySeparatorChar;
                }

                if (solutionDatabaseProjectFolder.Length > 0 && !solutionDatabaseProjectFolder.EndsWith(Path.DirectorySeparatorChar))
                {
                    solutionDatabaseProjectFolder += Path.DirectorySeparatorChar;
                }

                if (!string.IsNullOrWhiteSpace(solutionViewModelsScript))
                {
                    solutionViewModelsScript = solutionViewModelsScript.Replace("{solutionProjectsFolder}", solutionProjectsFolder);
                }

                envVars.Add("SOLUTION_PROJECTS_FOLDER", $"{solutionProjectsFolder}");
                envVars.Add("DATABASE_PROJECT_FOLDER", $"{solutionDatabaseProjectFolder}");
            }
            envVars.Add("CONNECTION_STRING", $"server={mySqlServerHost};database={mySqlDatabaseName};uid={mySqlAdminUsername};{(mySqlAdminPassword == "[NONE]" ? "" : $"password={mySqlAdminPassword}")}");

        }

        private void SyncProjectFromDatabase()
        {
            string arguments;

            //(1) Sync the project with changes you've made from your local database
            Console.WriteLine($"Scanning source database - {mySqlDatabaseName}...");
            arguments = $"--server={mySqlAdminUsername}{(mySqlAdminPassword == "[NONE]" ? "" : $":{mySqlAdminPassword}")}@{mySqlServerHost} --export=DEFINITIONS {mySqlDatabaseName} --skip=CREATE_DB,DATA --output-file=_temp_{mySqlDatabaseName}_sync_project.sql";
            string stdout = RunProcess("mysqldbexport", arguments);
            Console.WriteLine($"Source Database scanned.");

            Console.WriteLine($"Updating project from database");
            string tempSql = File.ReadAllText($"_temp_{mySqlDatabaseName}_sync_project.sql");

            string tableRegEx = @"# TABLE: `.*`\.`(.*)`.*\n(CREATE TABLE [^#]*)";            
            var directory = $"{solutionProjectsFolder}{solutionDatabaseProjectFolder}Tables{Path.DirectorySeparatorChar}";
            Directory.EnumerateFiles(directory).ToList().ForEach(f => File.Delete(f));
            foreach (Match match in Regex.Matches(tempSql, tableRegEx))
            {
                File.WriteAllText($"{directory}{match.Groups[1].Value}.sql", match.Groups[2].Value);
            }

            (new[] { "PROCEDURE", "FUNCTION", "TRIGGER" }).ToList().ForEach(type =>
            {
                string regEx = $@"# {type}: `.*`\.`(.*)`.*\n(DELIMITER \|\|[\w\W]*?DELIMITER ;)";
                directory = $"{solutionProjectsFolder}{solutionDatabaseProjectFolder}{type.Transform(To.LowerCase, To.TitleCase)}s{Path.DirectorySeparatorChar}";
                Directory.EnumerateFiles(directory).ToList().ForEach(f => File.Delete(f));
                foreach (Match match in Regex.Matches(tempSql, regEx))
                {
                    File.WriteAllText($"{directory}{match.Groups[1].Value}.sql", match.Groups[2].Value.Replace(" DEFINER=`root`@`localhost`", string.Empty).Replace($" DEFINER=`{mySqlAdminUsername}`@`{mySqlServerHost}`", string.Empty));
                }
            });

            Console.WriteLine("Generate the ReferenceData scripts...");
            arguments = $"script {solutionDatabaseProjectReferenceDataScript}";
            RunProcess("dotnet", arguments);
            Console.WriteLine("ReferenceData scripts generated");

            Console.WriteLine($"Finished updating the project from {mySqlDatabaseName}");
        }

        #region Publish to the DB

        private void PublishTheProjectToLocalDatabase(bool runScripts = true)
        {
            this.runScripts = runScripts;
            if (!DatabaseExists(mySqlDatabaseName))
            {
                CreateTheTargetDatabase();
            }
            else
            {
                ExecuteScriptsInFolder("Pre-Deployment");
            }

            CreateTemporaryComparisonDatabase();

            if (CreateDeploymentScript())
            {
                if (runScripts)
                {
                    ExecuteDeploymentScript();
                }
            }

            ExecuteScriptsInFolder("Post-Deployment");

            ExecuteScriptsInFolder("ReferenceData");

            CreateUsersAndPermissions();
        }

        private void ExecuteScriptsInFolder(string folder)
        {
            Console.WriteLine($"Starting {folder} scripts for {mySqlDatabaseName}");
            StringBuilder scipts = new StringBuilder();
            scipts.AppendLine($"USE `{mySqlDatabaseName}`;");

            foreach (string script in Directory.GetFiles($"Scripts{Path.DirectorySeparatorChar}{folder}{Path.DirectorySeparatorChar}", "*.sql"))
            {
                scipts.AppendLine(File.ReadAllText(script));
            }

            File.WriteAllText($"_temp_{mySqlDatabaseName}_{folder.Transform(To.LowerCase)}.sql", scipts.ToString());

            if (runScripts)
            {
                Console.WriteLine("Executing Pre-Deployment Script");
                string query = $"source _temp_{mySqlDatabaseName}_{folder.Transform(To.LowerCase)}.sql";
                RunMySqlQuery(query);
                Console.WriteLine($"{folder} scripts complete");
            }
        }

        private void CreateTheTargetDatabase()
        {
            Console.WriteLine($"Target database {mySqlDatabaseName} doesn't exist. Creating...");
            string query = $"CREATE DATABASE {mySqlDatabaseName}";
            RunMySqlQuery(query);
            Console.WriteLine($"Target database {mySqlDatabaseName} created");
        }

        private void CreateTemporaryComparisonDatabase()
        {
            // Execute the deployment deploying the new schema to a temp DB and then comparing new to old
            Console.WriteLine($"Starting Database Deployment for {mySqlDatabaseName}");
            StringBuilder deploymentScripts = new StringBuilder();

            deploymentScripts.AppendLine("SET FOREIGN_KEY_CHECKS = 0;");
            deploymentScripts.AppendLine($"USE `{mySqlDatabaseName}`;");

            //TABLES
            foreach (string script in Directory.GetFiles($"Tables{Path.DirectorySeparatorChar}", "*.sql"))
            {
                deploymentScripts.AppendLine(File.ReadAllText(script));
            }
            //STORED PROCS
            foreach (string script in Directory.GetFiles($"Procedures{Path.DirectorySeparatorChar}", "*.sql"))
            {
                deploymentScripts.AppendLine(File.ReadAllText(script));
            }
            //FUNCTIONS
            foreach (string script in Directory.GetFiles($"Functions{Path.DirectorySeparatorChar}", "*.sql"))
            {
                deploymentScripts.AppendLine(File.ReadAllText(script));
            }
            //TRIGGERS
            foreach (string script in Directory.GetFiles($"Triggers{Path.DirectorySeparatorChar}", "*.sql"))
            {
                deploymentScripts.AppendLine(File.ReadAllText(script));
            }
            deploymentScripts.AppendLine("SET FOREIGN_KEY_CHECKS = 1;");

            string fileSql = deploymentScripts.ToString().Replace($"`{mySqlDatabaseName}`", $"`{mySqlDatabaseName}_comparison`");
            File.WriteAllText($"_temp_{mySqlDatabaseName}_comparison_deployment.sql", fileSql);

            Console.WriteLine("DROP temp comparison database if it already exists...");
            string query = $"SET FOREIGN_KEY_CHECKS = 0; DROP DATABASE IF EXISTS {mySqlDatabaseName}_comparison; SET FOREIGN_KEY_CHECKS = 1;";
            RunMySqlQuery(query);
            Console.WriteLine("Temp comparison database deleted");

            Console.WriteLine("CREATE temp database for comparison...");
            query = $"CREATE DATABASE {mySqlDatabaseName}_comparison";
            RunMySqlQuery(query);
            Console.WriteLine("Temp comparison database created");

            Console.WriteLine("Publish temp comparison database...");
            query = $"source _temp_{mySqlDatabaseName}_comparison_deployment.sql";
            RunMySqlQuery(query);
            Console.WriteLine("Temp comparison database created");

        }

        private bool CreateDeploymentScript()
        {
            Console.WriteLine("Get the diff between source database and target database...");
            string arguments = $"--difftype=sql --force --skip-table-options --server1={mySqlAdminUsername}{(mySqlAdminPassword == "[NONE]" ? "" : $":{mySqlAdminPassword}")}@{mySqlServerHost} " +
                $"--server2={mySqlAdminUsername}{(mySqlAdminPassword == "[NONE]" ? "" : $":{mySqlAdminPassword}")}@{mySqlServerHost} --changes-for=server2 {mySqlDatabaseName}_comparison:{mySqlDatabaseName}";

            string stdout = RunProcess("mysqldiff", arguments, new[] { 0, 1 });

            string checkForNewObjectsRegEx = $@"\# WARNING: Objects in server1\.{mySqlDatabaseName}_comparison but not in server1\.{mySqlDatabaseName}\:";
            Console.WriteLine($"Looking for \"{checkForNewObjectsRegEx}\"");

            stdout = Regex.Replace(stdout, @"\[FAIL\]\r?\n\r?\n# WARNING: Cannot generate SQL[\s\W\w]*?No changes required or not supported difference.", string.Empty);

            string deploymentScript = string.Empty;
            if (Regex.IsMatch(stdout, checkForNewObjectsRegEx))
            {
                Console.WriteLine("New Objects detected that aren't in the target DB");

                StringBuilder newTablesBuilder = new StringBuilder();
                StringBuilder newProgramsBuilder = new StringBuilder();
                StringBuilder newTriggersBuilder = new StringBuilder();

                string newDbObjectRegEx = @"^#\s*((?:TABLE)|(?:TRIGGER)|(?:FUNCTION)|(?:PROCEDURE))\:\s([a-zA-Z0-9_]*)";

                foreach (Match match in Regex.Matches(stdout, newDbObjectRegEx, RegexOptions.Multiline))
                {
                    string objectType = match.Groups[1].Value.Transform(To.LowerCase, To.TitleCase);
                    string objectName = match.Groups[2].Value;
                    string script = $"{objectType}s{Path.DirectorySeparatorChar}{objectName}.sql";
                    Console.WriteLine($"Reading file {script}");
                    switch (objectType)
                    {
                        case "Table":
                            newTablesBuilder.AppendLine(File.ReadAllText(script));
                            break;
                        case "Trigger":
                            newTriggersBuilder.AppendLine(File.ReadAllText(script));
                            break;
                        case "Function":
                        case "Procedure":
                            newProgramsBuilder.AppendLine(File.ReadAllText(script));
                            break;
                    }
                }

                deploymentScript = $"USE `{mySqlDatabaseName}`;\r\nSET FOREIGN_KEY_CHECKS = 0;\r\n{newTablesBuilder.ToString()}\r\n{newProgramsBuilder.ToString()}\r\n{newTriggersBuilder.ToString()}\r\nSET FOREIGN_KEY_CHECKS = 1;\r\n";
            }

            if (stdout.IndexOf("Compare failed. One or more differences found.") > 0)
            {
                deploymentScript += $"\r\n{stdout}";
            }

            deploymentScript = Regex.Replace(deploymentScript, @"^#\s.*\r?\n", string.Empty, RegexOptions.Multiline);

            if (!string.IsNullOrWhiteSpace(deploymentScript))
            {
                // There were differences found, let's update the target
                File.WriteAllText($"_temp_{mySqlDatabaseName}_deployment.sql", deploymentScript.Replace("_comparison", ""));
                Console.WriteLine("Diff script created");
                return true;
            }
            else
            {
                Console.WriteLine($"No changes detected for {mySqlDatabaseName}.");
                return false;
            }            
        }

        private void ExecuteDeploymentScript()
        {
            Console.WriteLine($"Update the target database {mySqlDatabaseName}...");
            string query = $"source _temp_{mySqlDatabaseName}_deployment.sql";
            RunMySqlQuery(query);
            Console.WriteLine($"Target database {mySqlDatabaseName} updated.");
        }

        private bool DatabaseExists(string databaseName)
        {
            string query = $"SELECT CASE WHEN EXISTS (SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '{databaseName}') THEN 'true' ELSE 'false' END AS 'DatabaseExists'";

            bool databaseExists = bool.Parse(RunMySqlQuery(query).Trim());

            Console.WriteLine($"Database {mySqlDatabaseName} exists: '{databaseExists}'");

            return databaseExists;
        }

        private void CreateUsersAndPermissions()
        {
            string fileSql;
            if (mySqlCreateUsers)
            {
                Console.WriteLine("Creating Users");
                StringBuilder userScripts = new StringBuilder();
                foreach (string script in Directory.GetFiles($"Users{Path.DirectorySeparatorChar}", "*.sql"))
                {
                    userScripts.AppendLine(File.ReadAllText(script));
                }
                fileSql = userScripts.ToString();
                File.WriteAllText($"_temp_{mySqlDatabaseName}_users.sql", fileSql);
                if (runScripts)
                {
                    RunMySqlQuery($"source _temp_{mySqlDatabaseName}_users.sql");
                }
                Console.WriteLine("Creating Users complete");
            }

            Console.WriteLine("Creating Grant permissions");
            StringBuilder grantScripts = new StringBuilder();
            foreach (string script in Directory.GetFiles($"Grants{Path.DirectorySeparatorChar}", "*.sql"))
            {
                grantScripts.AppendLine(File.ReadAllText(script));
            }
            fileSql = grantScripts.ToString();
            File.WriteAllText($"_temp_{mySqlDatabaseName}_grants.sql", fileSql);
            if (runScripts)
            {
                RunMySqlQuery($"source _temp_{mySqlDatabaseName}_grants.sql");
            }
            Console.WriteLine("Grant permissions complete");
        }

        #endregion

        private void GenerateModels()
        {
            string arguments;

            Console.WriteLine("Generate the Context and DomainModel...");
            arguments = $"script {solutionDatabaseProjectEntitiesScript}";
            RunProcess("dotnet", arguments);
            Console.WriteLine("Context and DomainModel generated");

            Console.WriteLine("Generate the ViewModels...");
            arguments = $"script {solutionViewModelsScript}";
            RunProcess("dotnet", arguments);
            Console.WriteLine("ViewModels generated");

        }

        private string RunProcess(string executable, string arguments, int[] acceptableExitCodes = null)
        {
            if (acceptableExitCodes == null)
            {
                acceptableExitCodes = new[] { 0 };
            }

            int exitCode = 0;
            var stdOutput = new StringBuilder();
            var errOutput = new StringBuilder();
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = executable;
            startInfo.Arguments = arguments;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            foreach(string key in envVars.Keys)
            {
                startInfo.EnvironmentVariables.Add(key, envVars[key]);                
            }
            process.StartInfo = startInfo;
            Console.WriteLine($"{startInfo.FileName} {startInfo.Arguments}");
            
            //create event handler
            process.OutputDataReceived += new DataReceivedEventHandler(
                (s, e) =>
                {
                    stdOutput.AppendLine(e.Data);
                }
            );
            process.ErrorDataReceived += new DataReceivedEventHandler(
                (s, e) =>
                {
                    errOutput.AppendLine(e.Data);
                }
            );
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();

            exitCode = process.ExitCode;

            if (Array.IndexOf(acceptableExitCodes, exitCode) == -1)
            {
                Console.WriteLine($"Error encountered.\nCode: {exitCode}\nError: {errOutput.ToString()}");
#if DEBUG
                if (!unattended)
                {
                    Console.WriteLine("Error Encountered. Press any key to continue...");
                    Console.ReadKey();
                }
#endif
                Environment.Exit(exitCode);
            }
            else
            {
                Console.WriteLine(stdOutput.ToString());
            }
            return stdOutput.ToString();
        }

        private string RunMySqlQuery(string query)
        {
            string arguments;
            if (string.IsNullOrEmpty(mySqlExe))
            {
                if (isWindows)
                {
                    Console.WriteLine("Getting local MySQL server information...");
                    arguments = $"--server={mySqlAdminUsername}{(mySqlAdminPassword == "[NONE]" ? "" : $":{mySqlAdminPassword}")}@{mySqlServerHost} --format=vertical";

                    string stdout = RunProcess("mysqlserverinfo", arguments);
                    string mySqlBaseDir = Regex.Match(stdout, @"basedir: ([^\r]*)?\r?\n").Groups[1].Value;
                    Console.WriteLine($"MySQL base directory is {mySqlBaseDir}");
                    mySqlExe = $"{mySqlBaseDir}bin{Path.DirectorySeparatorChar}mysql.exe";
                }
                else
                {
                    mySqlExe = $"mysql";
                }
                Console.WriteLine($"MySQLExe is {mySqlExe}");
            }

            arguments = $"-u {mySqlAdminUsername}{(mySqlAdminPassword == "[NONE]" ? "" : $" -p{mySqlAdminPassword}")} -h {mySqlServerHost} -N -s -e \"{query}\"";
            return RunProcess(mySqlExe, arguments);
        }

        private string FindProjectsDirectory(string folder)
        {
            Console.WriteLine($"Folder is {folder}");
            if (folder.Length <= 3)
                return string.Empty; //we couldn't find it - just return nothing.

            if (folder.EndsWith($"{Path.DirectorySeparatorChar}projects"))
            {
                return folder;
            }
            else
            {
                return FindProjectsDirectory(Directory.GetParent(folder).FullName);
            }
        }

    }
}
