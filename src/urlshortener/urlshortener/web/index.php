<?php
    require_once('config.php')
    // TODO: Add package management
?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>URL Shortener</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/css/site.css">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="assets/js/clipboard.min.js"></script>
        <script src="assets/js/urlshortener_webapi.js"></script>
        <script>
            window.apiConfig = {'host': '<?php echo API_HOST ?>', 'port': '<?php echo API_PORT ?>'};
        </script>
    </head>
    <body>
        <div class="container">
            <div class="header clearfix">
                <nav>
                <ul class="nav nav-pills pull-right">
                    <li role="presentation" class="active"><a href="/index.php">Home</a></li>
                    <li role="presentation"><a href="/about.php">About</a></li>
                    <li role="presentation"><a href="/contact.php">Contact</a></li>
                </ul>
                </nav>
                <h3 class="text-muted">URL Shortener</h3>
            </div>

            <div class="jumbotron">
                <h1>Shorten Your URL Here!</h1>
                <p class="lead">Just paste in the URL you would like to shorten below. We will even throw in a complimentary anti-phishing check!</p>            
                <form class="form-signin" id="urlshortenerform">
                    <input type="text" id="url" name="url" class="form-control" placeholder="https://some.crazy-long-domain.com/really-long-url-i-want-shortened#with-some-stuff-at-the-end" required autofocus>
                    <div class="row">
                        <div class="col-lg-5"> </div>                    
                        <div class="col-lg-2"><button class="btn btn-lg btn-success" type="submit" id="shortensubmit">Shorten it!</button></div>                    
                        <div class="col-lg-5"> </div>
                    </div>
                </form>   
                    <div class="row">             
                        <div class="col-lg-5"> </div>                    
                        <div class="col-lg-2 form-control" id="shortenedurl" data-clipboard-text=""> </div>
                        <div class="col-lg-5"> </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5"> </div>                    
                        <div class="col-lg-2"><div class="btn btn-lg btn-success" id="copied">Copied!</div></div>                    
                        <div class="col-lg-5"> </div>
                    </div>
                </div>
            </div>

            <footer class="footer">
                    <div class="row">
                        <div class="col-lg-5"> </div>                    
                        <div class="col-lg-2"><p>That was a fun project guys, thank you.</p></div>                    
                        <div class="col-lg-5"> </div>
                    </div>            
            </footer>
        </div> <!-- /container -->
    </body>
</html>