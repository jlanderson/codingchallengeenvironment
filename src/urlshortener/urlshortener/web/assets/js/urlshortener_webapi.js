function initApp(){
    var clipboard = new ClipboardJS('#shortenedurl');
    clipboard.on('success', function(e) {
        $('#copied').show();
    });
    $('#urlshortenerform').submit(function(e) {
        var form = $(this);
        var webApiUrl = `//${window.apiConfig.host}:${window.apiConfig.port}/shorten`;
        $.ajax({
               type: "POST",
               url: webApiUrl,
               data: JSON.stringify({'url': $('#url').val()}), // serializes the form's elements.
               contentType: "application/json",
               dataType: "json",               
               success: function(data)
               {
                   $('.jumbotron h1').first().text('Your new shortened URL:');
                   $('.jumbotron p.lead').first().text('Just click in the box to copy to the clipboard.');
                   $('#urlshortenerform').hide();
                   $('#shortenedurl').text(data.url).attr('data-clipboard-text', data.url).show();
               },
               error: function(xhr, status){
                if (xhr.responseJSON && xhr.responseJSON.message){
                    $('#urlshortenerform').hide();
                    $('#shortenedurl').text(xhr.responseJSON.message).show();                    
                }
               }
             });    
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
}

$(document).ready(initApp);