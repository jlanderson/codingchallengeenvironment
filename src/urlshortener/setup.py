import json
from distutils.core import setup

install_requires = [
    'aiohttp',
    'aiohttp_cors',
    'ijson',
    'mysql-connector',
    'pyyaml'
]

setup(name='urlshortener',
      version='0.0.1',
      description='Url shortener using aiohttp',
      install_requires=install_requires)